    :::python
    import abc
    
-------


    #!javascript
    let a = 2;
    function()


-------

* List item

        #!css
        .post-list li .author a,
        .post-list li .author a:hover {
            color: #F30;
            text-transform: uppercase;
        }

* Another list item


* List item

    ```css

    .post-list li .author a,
    .post-list li .author a:hover {
        color: #F30;
        text-transform: uppercase;
    }
    ```

-------


```
#!python

def func():
    pass
```

-------

```
::python

def func():
    pass
```



The HTML specification
is maintained by the W3C.

*[HTML]: Hyper Text Markup Language
*[W3C]:  World Wide Web Consortium


```python
def hilite(self):
        """
        returns : A string of html.
        """

        self.src = self.src.strip('\n')

        if self.lang is None:
            self._parseHeader()

        if pygments and self.use_pygments:
            try:
                lexer = get_lexer_by_name(self.lang)
            except ValueError:
                pass
```

-------


```sql
select * from a
```

-------

```python
select * from a
```
-------

```javascript
import x
```

-------

```python
import x
```

-------

```python
let a = 5
```

-------

```javascript
let a = 5
```


